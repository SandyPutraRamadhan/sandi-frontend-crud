import Login from './component/Login';
import Register from './component/Register'
import Home from './view/Home';
import Category from './component/Category';
import Dashboard from './component/Dashboard'
import Makanan from './Menu/List';
import Edit from './view/EditList';
import {  Route, Routes  } from 'react-router-dom';
import Games from './Menu/Games';
import Cart from './component/Cart';
import User from './Menu/User';
import EditUser from './view/EditUser';

function App() {
  return (
    <div>
    <div>
      <Routes>
        <Route path='/login' element={<Login/>}/>
        <Route path='/register' element={<Register/>}/>
        <Route path='/category' element={<Category/>}/>
        <Route path='/' element={<Home/>}/>
        <Route path='/list' element={<Makanan/>}/>
        <Route path='/games' element={<Games/>}/>
        <Route path='/akun' element={<User/>}/>
        <Route path='/dashboard' element={<Dashboard/>}/>
        <Route path='/cart' element={<Cart/>}/>
        <Route path="/edit/:id" element={<Edit/>}/>
        <Route path="/editUser/:id" element={<EditUser/>}/>
      </Routes>
    </div>
    </div>
  );
}

export default App;
