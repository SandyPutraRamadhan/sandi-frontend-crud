import axios from "axios";
import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import "../style/Dashboard.css";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";
import Sidebar from "./Sidebar";
import Footer from "./Footer";
import { Button } from "bootstrap";
import Dashboards from "../style/Dashboard.css";
import Navbar from "./Navbar";

function Dashboard() {
  const [makanans, setMakanan] = useState([]);
  const [minuman, setMinumans] = useState([]);
  const [totalPage, setTotalPage] = useState([]);
  const [search, setSearch] = useState([]);

  // Get makanan
  const getAllMakanan = async (page = 0) => {
    await axios
      .get(`http://localhost:3006/products?page=${page}&search=${search}`)
      .then((response) => {
        const pages = [];
        for (let i = 0; i < response.data.data.totalPages; i++) {
          pages.push(i);
        }
        setTotalPage(pages);
        setSearch(search);
        setMakanan(
          response.data.data.content.map((data) => ({
            ...data,
            quantity: 1,
          }))
        );
      })
      .catch((error) => {
        console.log("Terjadi Kesalahan " + error);
      });
  };
  // Get minuman
  //   const getAllMinuman = async () => {
  //     await axios
  //   .get("http://localhost:8000/Minuman")
  //   .then((response) => {
  //     setMinumans(response.data);
  //   })
  //   .catch((error) => {
  //     console.log("Terjadi Kesalahan " + error);
  //   });
  // }
  const addToCart = async (data) => {
    Swal.fire({
      position: "center-center",
      icon: "success",
      title: "Berhasil Memasukkan Ke Keranjang",
      showConfirmButton: false,
      timer: 1500,
    });
    await axios.post("http://localhost:3006/cart", {
      products: data.id,
      quentity: data.quantity,
      users: localStorage.getItem("userId"),
    });
  };
  useEffect(() => {
    getAllMakanan();
    // getAllMinuman();
  }, []);
  const plusQuantityProducts = (id) => {
    const listProducts = [...makanans];
    if (listProducts[id].quantity >= 45) return;
    listProducts[id].quantity++;
    setMakanan(listProducts);
  };
  const minQuantityProducts = (id) => {
    const listProducts = [...makanans];
    if (listProducts[id].quantity <= 1) return;
    listProducts[id].quantity--;
    setMakanan(listProducts);
  };
  return (
    <div className="bg-gray-500">
      <Navbar />
      <div className="dashboard">
        <Sidebar />
        <Carousel>
          <div>
            <img src="https://cdn-2.tstatic.net/tribunnews/foto/bank/images/daftar-promo-hari-ini-kfc-hingga-richeese-factory.jpg" />
          </div>
          <div>
            <img src="https://cdn-2.tstatic.net/pontianak/foto/bank/images/promo-makanan-hari-ini-12-juli-2021.jpg" />
          </div>
          <div>
            <img src="https://cdn-2.tstatic.net/tribunnews/foto/bank/images/promo-minuman-oktober.jpg" />
          </div>
        </Carousel>
      </div>
      <h1>Daftar Menu</h1>
      <div class="d-flex p-4 ml-[250px]" role="search">
        <input
          class="form-control me-2 "
          type="search"
          placeholder="Cari Menu Makanan Anda"
          aria-label="Search"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        <button class="btn btn-outline-danger" onClick={() => getAllMakanan()}>
          Search
        </button>
      </div>
      <div
        style={{ padding: 20, display: "flex", gap: 50, marginLeft: 240 }}
        className="flex-wrap"
      >
        {makanans.map((makanans, id) => (
          <div
            class="card"
            style={{
              width: "16rem",
              background: "PaleGoldenRod",
              padding: 3,
            }}
          >
            <img
              style={{ width: 250, height: 200 }}
              src={makanans.img}
              class="card-img-top"
              alt="..."
            />
            <div class="card-body" style={{ textAlign: "center" }}>
              <h5 class="card-title">{makanans.nama}</h5>
              <p class="card-text">{makanans.deskripsi}</p>
              <h6>Rp.{makanans.harga}</h6>

              <button
                type="button"
                onClick={() => minQuantityProducts(id)}
                className={`w-4 h-10 leading-4 transition hover:opacity-75 ${
                  makanans.quantity <= 1 ? "grey" : "green"
                }`}
              >
                -
              </button>

              <input
                type="number"
                id="Quantity"
                value={makanans.quantity}
                class="h-10 w-16 border-transparent text-center [-moz-appearance:_textfield] sm:text-sm [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none"
              />

              <button
                type="button"
                onClick={() => plusQuantityProducts(id)}
                className={`w-4 h-10 leading-4 transition hover:opacity-75 ${
                  makanans.quantity >= 45 ? "grey" : "green"
                }`}
              >
                +
              </button>
              <hr />
              {localStorage.getItem("role") !== "USER" ? (
                <>
                <a href="/Registrasi" class="btn btn-danger">
                  For User
                </a>
                </>
              ) : (
                  <button
                    onClick={() => addToCart(makanans)}
                    className="btn btn-primary"
                  >
                    Buy
                  </button>
              )}
            </div>
            <div></div>
          </div>
        ))}
      </div>
      {/* <h1>Minuman</h1> */}
      {/* <div
        style={{ padding: 20, display: "flex", gap: 50 }}
        className="flex-wrap"
      >
        {minuman.map((drink) => (
          <div
            class="card"
            style={{
              width: "16rem",
              backgroundColor: "palegoldenrod",
              padding: 3,
            }}
          >
            <img
              style={{ width: 250, height: 200 }}
              src={drink.image}
              class="card-img-top"
              alt="..."
            />
            <div class="card-body" style={{ textAlign: "center" }}>
              <h5 class="card-title">{drink.nama}</h5>
              <p class="card-text">{drink.deskripsi}</p>
              <h6>Rp.{drink.harga}</h6>
              {localStorage.getItem("id") !== null ? (
               <>
                <a href="/login"  class="btn btn-danger">
                Login
               </a>
               </>
              ) : (
                <button className="btn btn-primary">Buy</button>      
              )}
            </div>
          </div>
        ))}
      </div> */}
      <ol class="flex justify-center gap-1 text-xs font-medium">
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Prev Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>

        {totalPage.map((e, i) => (
          <li>
            <span
              onClick={() => {
                getAllMakanan(i);
              }}
              key={i}
              class="cursor-pointer block h-8 w-8 rounded border border-gray-100 text-center leading-8"
            >
              {i + 1}
            </span>
          </li>
        ))}
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Next Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>
      </ol>
      <Footer />
    </div>
  );
}

export default Dashboard;
