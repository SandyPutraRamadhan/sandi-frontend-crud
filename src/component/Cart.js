import React, { useState, useEffect } from 'react'
import Swal from 'sweetalert2';
import axios from 'axios';
import { Button } from 'bootstrap';
import Navbar from './Navbar';
import Footer from './Footer';
import Sidebar from './Sidebar';
function Cart() {
  const [ makanans , setMakanan ] = useState([]);
  const [ totalPrice, setTotalPrice ] = useState([]);
  const [ totalPage, setTotalPage] =  useState([]);
  
     // Get
     const getAllMakanan = async (page = 0) => {
      await axios
    .get(`http://localhost:3006/cart?page=${page}&usersId=${localStorage.getItem("userId")}`)
    .then((response) => {
      const pages = []
        for (let i = 0; i < response.data.data.totalPages; i++) {
          pages.push(i)
         }
         setTotalPage(pages)
      setMakanan(response.data.data.content.map((e, i) => ({
        ...e,
        no:i+1,
      })));
      let total = 0;
      response.data.data.content.forEach((data) => {
        total += data.totalPrice;
      });
      setTotalPrice(total);
    })
    .catch((error) => {
      console.log("Terjadi Kesalahan " + error);
    });
  }
  const konverensi = (angka) => {
    return new Intl.NumberFormat("id-ID" ,{
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
    }).format(angka);
  }
  // Delete
  const deleteCart = async (id) => {
    Swal.fire({
        title: 'Apakah Yakin Ingin Delete?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Tetap Delete!'
      }).then((result) => {
        if (result.isConfirmed) {
          axios.delete("http://localhost:3006/cart/" + id)
          Swal.fire({
            icon:"success",
            title:"Berhasil Delete",
            showConfirmButton:false,
            timer:1500
          }
          );
          setTimeout(() => {
            window.location.reload();
          }, 1500)
          }
        })
      };
      const checkout = async() => {
      const confirm=  await  Swal.fire({
          title: 'Apakah Yakin Ingin Checkout?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Tetap Checkout!'
        })
        if (!confirm.isConfirmed) {
          return 
        }
          await axios.delete(`http://localhost:3006/cart?userId=${localStorage.getItem("userId")}`)

          Swal.fire({
            icon:"success",
            title:"Berhasil Checkout", 
            showConfirmButton:false,
            timer:1500
          }
          );
          setTimeout(() => {
            window.location.reload();
          }, 1500)
      }
  useEffect(() => {
    getAllMakanan();
  }, []);
  return (
   <div>
    <Navbar />
    <Sidebar />
    <br />
    <div style={{marginLeft: 250, padding: 10}}>
    <table className='table table-striped table-hover'>
        <thead>
          <th>No</th>
          <th>Nama</th>
          <th>Deskrpsi</th>
          <th>Image</th>
          <th>Jumlah</th>
          <th>Harga</th>
          <th>Action</th>
        </thead>
        <tbody >
          {makanans.map((data, index) => {
            return (
              <tr key={data.id}>
                <td>{index + 1}</td>
                <td>{data.products.nama}</td>
                <td>{data.products.deskripsi}</td>
                <td><img src={data.products.img} alt="" width={200} /></td>
                <td>{data.quentity}</td>
                <td>{konverensi(data.products.harga)}</td>
                <td>
                  <button
                    className="mx-1 btn btn-danger"
                    onClick={() => deleteCart(data.id)}
                  >
                    Hapus
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
      <ol class="flex justify-center gap-1 text-xs font-medium">
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Prev Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>

  
        {
          totalPage.map((e, i) => (
            <li>
        <span
          onClick={() => {
              getAllMakanan(i)
          }}
          key={i}
          class="cursor-pointer block h-8 w-8 rounded border border-gray-100 text-center leading-8"
        >
          {i + 1}
        </span>
      </li>
          ))
        }
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Next Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>
      </ol>
      <div style={{marginLeft: 245, paddingLeft: 20}}>
      <button className='btn btn-primary'><i class="fa-solid fa-tags"></i> Total Harga : {konverensi(totalPrice)}</button>
      <button className='btn btn-warning' onClick={checkout}><i class="fa-solid fa-money-check-dollar"></i> Checkout</button>
      <a className='btn btn-danger float-right' href="/dashboard"><i class="fa-solid fa-right-from-bracket rotate-180"></i>Kembali</a>
      </div>
    <div>
      <Footer />
    </div>
   </div>
  )
}

export default Cart
