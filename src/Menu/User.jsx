import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { MDBCol, MDBContainer, MDBRow, MDBCard, MDBCardText, MDBCardBody, MDBCardImage, MDBBtn, MDBTypography } from 'mdb-react-ui-kit';

export default function User() {
    const [makanans, setMakanan] =  useState([]);
    const [ totalPage, setTotalPage] =  useState([]);
    const [ user , setUser] = useState([]);
    const getAllMakanan = async (page = 0) => {
    await axios
      .get(`http://localhost:3006/products?page=${page}&search=`)
      .then((response) => {
        const pages = []
        for (let i = 0; i < response.data.data.totalPages; i++) {
            pages.push(i)
           }
        setTotalPage(pages)
         setMakanan(response.data.data.content);
      })
      .catch((error) => {
        console.log("Terjadi Kesalahan " + error);
      });
  };
  const getUser = async () => {
    await axios.get("http://localhost:3006/user/" + localStorage.getItem("userId"))
    .then((res) => {
      setUser(res.data.data)
    }).catch((err) => {
      console.log(err);
    });
  }
  console.log(user);
  useEffect(() => {
    getAllMakanan();
    getUser();
  }, []);
  return (
    <div>
    <div className="gradient-custom-2 bg-gray-600">
      <MDBContainer className="py-5 h-100">
        <MDBRow className="justify-content-center align-items-center h-100">
          <MDBCol lg="9" xl="7" >
            <MDBCard>
              <div className="rounded-top text-white d-flex flex-row bg-gradient-to-b from-cyan-500 to-blue-500" style={{ height: '200px' }}>
                <div className="ms-4 mt-5 d-flex flex-column" style={{ width: '150px' }}>
                  <MDBCardImage src={user.foto}
                    alt="Generic placeholder image" className="mt-4 mb-2 img-thumbnail" fluid style={{ width: '150px', zIndex: '1' }} />
                </div>
                <div className="ms-3" style={{ marginTop: '130px' }}>
                  <MDBTypography tag="h2">{user.nama}</MDBTypography>
                  <MDBCardText>Email : {user.email}</MDBCardText>
                </div>
              </div>
              <div className="p-4 text-black" style={{ backgroundColor: '#f8f9fa' }}>
                <div className="d-flex justify-content-end text-center py-1">
                  <div>
                  <a
  className="inline-block rounded bg-indigo-600 px-8 py-3 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:bg-indigo-500 no-underline"
  href={"/editUser/" + user.id}
>
  Edit Profile
</a>
                  </div>
                </div>
              </div>
              <MDBCardBody className="text-black p-4">
                <div className="mb-5">
                  <h1 className="lead fw-normal mb-1">Blog Pribadi</h1>
                  <div className="p-4" style={{ backgroundColor: '#f8f9fa' }}>
                    <MDBCardText className="font-italic mb-1"><h4><i class="fa-solid fa-phone"></i> No Telepon <span className='float-right'>{user.telepon}</span></h4></MDBCardText>
                    <hr />
                    <MDBCardText className="font-italic mb-0"><h4><i class="fa-solid fa-location-dot"></i> Alamat <span className='float-right'>{user.alamat}</span></h4></MDBCardText>
                    <hr />
                    <MDBCardText className='font-italic mb-0'><h4><i class="fa-solid fa-user"></i> Sebagai <span className='float-right'>{user.role}</span></h4></MDBCardText>
                  </div>
                </div>
                <div className="d-flex justify-content-between align-items-center mb-4">
                  <MDBCardText className="lead fw-normal mb-0">Upload Photos</MDBCardText>
                  <MDBCardText className="mb-0"><a href="#!" className="text-muted">Show all</a></MDBCardText>
                </div>
                <MDBRow>
                    {makanans.map((data) => (
                  <MDBCol size="6" className="mb-2">
                    <MDBCardImage src={data.img}
                      alt="image 1" className="w-100 rounded-3 h-[300px]" />
                  </MDBCol>
                    ))}
                </MDBRow>
                <ol class="flex justify-center gap-1 text-xs font-medium">
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Prev Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clip-rule="evenodd"
                />
            </svg>
          </a>
        </li>

  
        {
          totalPage.map((e, i) => (
            <li>
        <span
          onClick={() => {
              getAllMakanan(i)
          }}
          key={i}
          class="cursor-pointer block h-8 w-8 rounded border border-gray-100 text-center leading-8"
        >
          {i + 1}
        </span>
      </li>
          ))
        }
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Next Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>
      </ol>
      <div>
      <a className='btn btn-danger float-right' href="/dashboard"><i class="fa-solid fa-right-from-bracket rotate-180"></i> Kembali</a>
      </div>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </div>
    </div>
  );
}