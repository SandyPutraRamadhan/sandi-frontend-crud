import axios from 'axios';
import React, { useState, useEffect } from 'react'
import Navbar from '../component/Navbar'
import Swal from 'sweetalert2'
import Sidebar from '../component/Sidebar'
import { Button, InputGroup, Modal, Form } from 'react-bootstrap';
import { number } from 'prop-types';
import { Navigate } from 'react-router-dom';
import Footer from '../component/Footer';

export default function Makanan() {
  const [show, setShow] = useState(false);
    const [ makanans , setMakanan ] = useState([]);
    const [nama, setNama] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [img, setImg] = useState();
  const [harga, setHarga] = useState(Number);
  const [click, setClick] = useState(false);
  const [ totalPage, setTotalPage] =  useState([]);
  const [ search, setSearch] = useState([]);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const addList = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData()
    formData.append("file", img)
    formData.append("nama", nama)
    formData.append("harga", harga)
    formData.append("deskripsi", deskripsi)
    try {
      await axios.post("http://localhost:3006/products", formData,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          'Content-Type': "multipart/form-data"
        },
      }
      );
    setShow(false)
    Swal.fire({
      icon: "success",
      title: "Berhasil Menambahkan",
    });
    setTimeout(() => {
      window.location.reload();
    }, 1500)
  } catch (error) {
    console.log(error);
  }
  }

  const logout= () => {
    window.location.reload();
    localStorage.clear();
    Navigate("/dashboard")
  };
    // Get
   
    const getAllMakanan = async (page = 0) => {
        await axios
      .get(`http://localhost:3006/products?page=${page}&search=${search}`)
      .then((response) => {
        const pages = []
        for (let i = 0; i < response.data.data.totalPages; i++) {
         pages.push(i)
        }
        setTotalPage(pages)
        setSearch(search)
        setMakanan(response.data.data.content);
      })
      .catch((error) => {
        console.log("Terjadi Kesalahan " + error);
      });
    }

    // Add 
    // const addMakanan = async (e) => {
    //   e.preventDefault();
  
    //   try {
    //     Swal.fire({
    //       icon: "success",
    //       title: "Berhasil Menambahkan",
    //     });
    //     await axios.post("http://localhost:8000/Makanans", {
    //       nama: nama,
    //       deskripsi: deskripsi,
    //       image: image,
    //       harga: Number(harga),
    //     });
    //   } catch (error) {
    //     console.log(error);
    //     Swal.fire({
    //       icon: "error",
    //       title: "Oops...",
    //       text: "Something went wrong!",
    //     });
    //   }
    //   window.location.reload();
    // };

    // Delete
    const deleteMakanan = async (id) => {
      Swal.fire({
          title: 'Apakah Yakin Ingin Delete?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Tetap Delete!'
        }).then((result) => {
          if (result.isConfirmed) {
            axios.delete("http://localhost:3006/products/" + id)
            Swal.fire(
              'Berhasil Men Delete!',
              'File Anda Telah Di Delete',
              'success'
              )
            }
            // window.location.reload();
          })
        };
    useEffect(() => {
      getAllMakanan();
    }, []);
  return (
   <div className='bg-gray-500'>
    <Navbar />
    <Sidebar />
    <div class="d-flex p-4 ml-[250px]" role="search">
        <input class="form-control me-2 "
         type="search"
         placeholder="Search"
         aria-label="Search"
         value={search}
         onChange={(e) => setSearch(e.target.value)}
          />
        <button class="btn btn-outline-success" onClick={() => getAllMakanan()}>Search</button>
      </div>
    {localStorage.getItem("role") === "ADMIN" ? (
      <button className="btn btn-danger float-right m-4" onClick={handleShow}>
                Add
    </button>
      ) : (
        <></>
    )
    }
    <div
        style={{ padding: 20, display: "flex", gap: 50, marginLeft: 240 }}
        className="flex-wrap bg-gray-500"
      >
        {makanans.map((makanans) => (
          <div
            class="card"
            style={{
              width: "16rem",
              backgroundColor: "PaleGoldenRod",
              padding: 3,
            }}
          >
            <img
              style={{ width: 250, height: 200 }}
              src={makanans.img}
              class="card-img-top"
              alt="..."
            />
            <div class="card-body" style={{ textAlign: "center" }}>
              <h5 class="card-title">{makanans.nama}</h5>
              <p class="card-text">{makanans.deskripsi}</p>
              <h6>Rp.{makanans.harga}</h6>
              <hr />
              {localStorage.getItem("role") === "ADMIN" ? (
                <>
                 <Button
                    variant="danger"
                    className="mx-1"
                    onClick={() => deleteMakanan(makanans.id)}
                  >
                     Delete
                  </Button>
                  <a href={"/edit/" + makanans.id}>
                    <Button variant="success" className="mx-1">
                      Edit
                    </Button>
                    </a>
                </>  
                ) : (
               <a href="/login"  class="btn btn-danger">
                Login
               </a>
              )}
            </div>
          </div>
        ))}
      </div>
<Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Tambahkan Makanan</Modal.Title>
          </Modal.Header>
          <form onSubmit={addList}>
            <Modal.Body>
              <div className="mb-3">
                <Form.Label>
                  <strong>Nama</strong>
                </Form.Label>
                <InputGroup className="d-flex gab-3">
                  <Form.Control
                    placeholder="Masukkan Nama"
                    value={nama}
                    onChange={(e) => setNama(e.target.value)}
                  />
                </InputGroup>
              </div>
              <div className="mb-3">
                <Form.Label>
                  <strong>Deskripsi</strong>
                </Form.Label>
                <InputGroup className="d-flex gab-3">
                  <Form.Control
                    placeholder="Masukkan Deskripsi"
                    value={deskripsi}
                    onChange={(e) => setDeskripsi(e.target.value)}
                  />
                </InputGroup>
              </div>
              <div className="mb-3">
                <Form.Label>
                  <strong>Image</strong>
                </Form.Label>
                <InputGroup className="d-flex gab-3">
                  <Form.Control
                    placeholder="Masukkan Image"
                    type='file'
                    onChange={(e) => setImg(e.target.files[0])}
                  />
                </InputGroup>
              </div>
              <div className="input">
                <Form.Label>
                  <strong>Harga</strong>
                </Form.Label>
                <InputGroup className="d-flex gab-3">
                  <Form.Control
                    placeholder="Masukkan Harga"
                    value={harga}
                    onChange={(e) => setHarga(e.target.value)}
                  />
                </InputGroup>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <button type="submit" className='btn btn-primary'>
                Save Changes
              </button>
            </Modal.Footer>
          </form>
        </Modal>
        <ol class="flex justify-center gap-1 text-xs font-medium">
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Prev Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>

  
        {
          totalPage.map((e, i) => (
            <li>
        <span
          onClick={() => {
              getAllMakanan(i)
          }}
          key={i}
          class="cursor-pointer block h-8 w-8 rounded border border-gray-100 text-center leading-8"
        >
          {i + 1}
        </span>
      </li>
          ))
        }
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Next Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>
      </ol>
      <Footer /> 
   </div>
  )
}
