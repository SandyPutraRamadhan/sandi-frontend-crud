import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useNavigate, useParams } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import Swal from 'sweetalert2'
import { Form, InputGroup } from "react-bootstrap";

export default function EditUser() {
    const param = useParams();
    const [ nama, setNama ] = useState("");
    const [ alamat, setAlamat ] = useState("");
    const [  telepon, setTelepon ] = useState("");
    const [ password, setPassword ] = useState("");
    const [ foto, setFoto ]= useState();

    const navigate = useNavigate();

    const updateUser = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("file", foto)
        formData.append("nama", nama)
        formData.append("alamat", alamat)
        formData.append("telepon", telepon)
        formData.append("password", password)
        Swal.fire({
            title: 'apakah yakin di edit datanya?',
            showCancelButton: true,
            confirmButtonText: 'Edit',
  }).then((result) => {
    if (result.isConfirmed) {
        axios.put("http://localhost:3006/user/" + param.id , formData, 
        {
            headers : {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "multipart/form-data",
              }
        }
        ).then(() => {
            navigate("/akun")
            Swal.fire('Berhasil Mengedit!', '', 'success')
        }).catch((error) => {
            console.log(error);
        })
    }
  })
    }
    useEffect(() => {
        axios.get("http://localhost:3006/user/" + param.id)
        .then((res) => {
            const newUser = res.data.data;
            setNama(newUser.nama)
            setAlamat(newUser.alamat)
            setTelepon(newUser.telepon)
            setPassword(newUser.password)
            setFoto(newUser.foto)
        })
        .catch((error) => {
            alert("Terjadi Kesalahan " + error)
        })
    }, []);

  return (
    <div className="edit mx-5">
        <div className="container my-5 bg-gradient-to-r from-violet-500 to-fuchsia-500 text-center">
      <Form onSubmit={updateUser}>
          <div className="mb-3">
            <Form.Label>
              <strong>Nama</strong>
            </Form.Label>
            <InputGroup className="d-flex gab-3">
              <Form.Control
                placeholder="Masukkan nama"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="place-of-birth mb-3">
            <Form.Label>
              <strong>Alamat</strong>
            </Form.Label>
            <InputGroup className="d-flex gab-3">
              <Form.Control
                placeholder="Masukkan Alamat"
                value={alamat}
                onChange={(e) => setAlamat(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="place-of-birth mb-3">
            <Form.Label>
              <strong>Telepon</strong>
            </Form.Label>
            <InputGroup className="d-flex gab-3">
              <Form.Control
                placeholder="Masukkan Telepon"
                value={telepon}
                onChange={(e) => setTelepon(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="place-of-birth mb-3">
            <Form.Label>
              <strong>Password</strong>
            </Form.Label>
            <InputGroup className="d-flex gab-3">
              <Form.Control
                placeholder="Masukkan Password"
                value={password}
                type="password"
                onChange={(e) => setPassword(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="place-of-birth mb-3">
            <Form.Label>
              <strong>Photos</strong>
            </Form.Label>
            <InputGroup className="d-flex gab-3">
              <Form.Control
              type="file"
              required
                onChange={(e) => setFoto(e.target.files[0])}
              />
            </InputGroup>
          </div>

        <div className="d-flex justify-content-end align-items-center mt-2">
          <button className="btn btn-primary" type="submit">
            Save
          </button>
        </div>
      </Form>
        </div>
    </div>
  )
}
