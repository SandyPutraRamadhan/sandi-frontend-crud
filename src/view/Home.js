import React, {useEffect, useState} from 'react'
import '../style/HeroSections.css'
import HeroSection from './HeroSections'
import Footer from '../component/Footer'
import axios from 'axios'
import '../style/Home.css'

export default function Home() {
  const [makanans, setMakanan] = useState([]);
  const [ totalPage , setTotalPage ] = useState([]);
  const getAllMakanan = async (page = 0) => {
    await axios
  .get(`http://localhost:3006/products?page=${page}`)
  .then((response) => {
    const pages = []
    for (let i = 0; i < response.data.data.totalPages; i++) {
     pages.push(i)
    }
    setTotalPage(pages)
    setMakanan(response.data.data.content);
  })
  .catch((error) => {
    console.log("Terjadi Kesalahan " + error);
  });
}
useEffect(() => {
  getAllMakanan();
}, []);
  return (
    <div className='home'>
      <HeroSection />
    </div>
  )
}
