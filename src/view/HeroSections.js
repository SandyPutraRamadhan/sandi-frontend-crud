import React from 'react';
import '../App.css';
import { Button } from '../component/Button';
import { Link } from 'react-router-dom';
import '../style/HeroSections.css';
import vidioBg from '../videos/video-1.mp4'

function HeroSection() {
  return (
    <div>
    <section className='hero'>
      <main className='content'>
        <h1>Welcome To <span className='bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text text-3xl font-extrabold text-transparent sm:text-6xl'>Warung Online</span></h1>
        <p>Silahkan Berbelanja Di Warung Online Kami</p>
        <a href="/dashboard" className='cta'>Beli Sekarang</a>
      </main>
    </section>
    </div>
    // <div className='hero-container'>
    //   <video src={vidioBg} autoPlay loop muted />
    //   <div class="mx-auto max-w-3xl text-center">
    //   <h1
    //     class="bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text text-3xl font-extrabold text-transparent sm:text-5xl"
    //   >
    //     Selamat Datang Di

    //     <span class="sm:block"> Warung Online </span>
    //   </h1>

    //   <div class="mt-8 flex flex-wrap justify-center gap-4">
    //     <a
    //       class="block w-full rounded border border-blue-600 bg-blue-600 px-12 py-3 text-sm font-medium text-white hover:bg-transparent hover:text-white focus:outline-none focus:ring active:text-opacity-75 sm:w-auto"
    //       href="/login"
    //     >
    //       Login
    //     </a>

    //     <a
    //       class="block w-full rounded border border-blue-600 px-12 py-3 text-sm font-medium text-white hover:bg-blue-600 focus:outline-none focus:ring active:bg-blue-500 sm:w-auto"
    //       href="/register"
    //     >
    //       Register
    //     </a>
    //   </div>
    //   </div>
    // </div>
  );
}

export default HeroSection;
